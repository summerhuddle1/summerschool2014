#include "CastTutor/CastCalculator.h"

#include <limits.h>
#include <gtest/gtest.h>
#include <cmath>

void assert_vec3_equal(const Vec3 & expected, const Vec3 & actual)
{
	ASSERT_DOUBLE_EQ(expected.x, actual.x);
	ASSERT_DOUBLE_EQ(expected.y, actual.y);
	ASSERT_DOUBLE_EQ(expected.z, actual.z);	
}

void assert_velocity(const Vec3 & expected, const Vec3 & acceleration, const Vec3 & currentVelocity, const double time)
{
	Vec3 actual = getVelocity(acceleration, currentVelocity, time);
	assert_vec3_equal(expected, actual);
}

void assert_speed(const double & expected, const double & x, const double & y, const double & z)
{
	ASSERT_DOUBLE_EQ(expected, getSpeed(Vec3(x, y, z)));
}

//======================================================================

TEST(GetSpeed, All_Positive)
{
	assert_speed(pow(27, 0.5), 3.0, 3.0, 3.0);
}

TEST(GetSpeed, All_Negative)
{
	assert_speed(pow(27, 0.5), -3.0, -3.0, -3.0);
}

TEST(GetSpeed, X_negative)
{
	assert_speed(pow(27, 0.5), -3.0, 3.0, 3.0);
}

TEST(GetSpeed, Y_negative)
{
	assert_speed(pow(27, 0.5), 3.0, -3.0, 3.0);
}

TEST(GetSpeed, Z_negative)
{
	assert_speed(pow(27, 0.5), 3.0, 3.0, -3.0);
}

TEST(GetSpeed, X_positive)
{
	assert_speed(pow(27, 0.5), 3.0, -3.0, -3.0);
}

TEST(GetSpeed, Y_positive)
{
	assert_speed(pow(27, 0.5), -3.0, 3.0, -3.0);
}

TEST(GetSpeed, Z_positive)
{
	assert_speed(pow(27, 0.5), -3.0, -3.0, 3.0);
}

//======================================================================

TEST(GetVelocity, All_Velocity_Positive)
{
	double time = 0.05;
	Vec3 accerleration(0.0, 0.0, 0.0);
	Vec3 currentVelocity(3.0, 3.0, 3.0);
	Vec3 expected(3.0, 3.0, 3.0);
	assert_velocity(expected, accerleration, currentVelocity, time);
}

TEST(GetVelocity, All_Velocity_Negative)
{
	double time = 0.05;
	Vec3 accerleration(0.0, 0.0, 0.0);
	Vec3 currentVelocity(-3.0, -3.0, -3.0);
	Vec3 expected(-3.0, -3.0, -3.0);
	assert_velocity(expected, accerleration, currentVelocity, time);
}

TEST(GetVelocity, All_Acceleration_positive)
{
	double time = 0.05;
	Vec3 expected((3 * timeInterval), (3 * timeInterval), (3 * timeInterval));
	Vec3 accerleration(3.0, 3.0, 3.0);
	Vec3 currentVelocity(0.0, 0.0, 0.0);
	assert_velocity(expected, accerleration, currentVelocity, time);
}

TEST(GetVelocity, All_Acceleration_negative)
{
	double time = 0.05;
	Vec3 expected(((-3) * timeInterval), ((-3) * timeInterval), ((-3) * timeInterval));
	Vec3 accerleration(-3.0, -3.0, -3.0);
	Vec3 currentVelocity(0.0, 0.0, 0.0);
	assert_velocity(expected, accerleration, currentVelocity, time);	
}

