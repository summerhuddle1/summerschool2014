#include <limits.h>
#include <gtest/gtest.h>
#include <cstring>
#include "WristbandMocks/MockSocketClient.h"
#include "Wristband/BMA150DataConverter.h"
#include "Wristband/WristbandQuad.h"

TEST(BMA150DataConverter, sends_to_correct_port_and_host)
{
	MockSocketClient mockSocket;
	BMA150DataConverter dataConverter(&mockSocket);

	const char * message = "host name";
	dataConverter.socket->Open(message, std::strlen(message));

	ASSERT_EQ(message, mockSocket._mockHostName);
	ASSERT_EQ(std::strlen(message), mockSocket._mockPort);
}

TEST(BMA150DataConverter, dataConverter_converts_hex_to_expected_int)
{
	MockSocketClient mockSocket;
	BMA150DataConverter dataConverter(&mockSocket);
	
	int id;
	short x,y,z;
	Quad expected(id=1, x=1, y=2, z=3);

	unsigned char bytes[] =      
	{
	    0x40, 0x00, // x
		0x80, 0x00, // y
		0xC0, 0x00, // z
	};
	
	Quad actual = dataConverter.ConvertBytesToQuad(bytes);
	
	ASSERT_EQ(expected, actual);
}

TEST(BMA150DataConverter, sends_quad_to_socket)
{
	MockSocketClient mockSocket;
	BMA150DataConverter dataConverter(&mockSocket);

	int id;
	short x,y,z;
	Quad expected(id=1, x=1, y=2, z=3);
	
	char bytes[] = // little endian
	{
		0x01, 0x00, 0x00, 0x00, // id
		0x01, 0x00,				// x
		0x02, 0x00,				// y
		0x03, 0x00,				// z
	};
	
	dataConverter.SendtoSocket(& expected);
	
	ASSERT_EQ(0, memcmp(bytes, mockSocket._mockData, 10));
}
