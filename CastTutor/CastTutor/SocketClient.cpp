#include "SocketClient.h"

#include <string.h> // memset()
#include <sys/time.h> // select() 
#include <stdlib.h> // exit()
#include <stdio.h> // printf
#include <unistd.h> // close

#define LOCAL_SERVER_PORT 1500


SocketClient::~SocketClient()
{
	close(_socketDescripter);
}

void SocketClient::OpenReceiverSocket()
{
	/* socket creation */
	_socketDescripter = socket(AF_INET, SOCK_DGRAM, 0);
	if (_socketDescripter < 0) 
	{
		printf("cannot open socket \n");
		exit(1);
	}

	/* bind local server port */
	struct sockaddr_in servAddr;
	servAddr.sin_family = AF_INET;
	servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servAddr.sin_port = htons(LOCAL_SERVER_PORT);
	int bindResult = bind (_socketDescripter, (struct sockaddr *) &servAddr,sizeof(servAddr));
	if (bindResult < 0) 
	{
		printf("cannot bind port number %d \n", 
		   LOCAL_SERVER_PORT);
		exit(1);
	}
}

void SocketClient::Receive(void *dataBuffer, size_t length)
{
	printf("waiting for data on port UDP %u\n", LOCAL_SERVER_PORT);

	/* receive message */
	struct sockaddr_in cliAddr;
	unsigned int cliLen = sizeof(cliAddr);
	int n = recvfrom(_socketDescripter, dataBuffer, length, 0, 
		 (struct sockaddr *) &cliAddr, &cliLen);
		 
	if (n < 0) 
	{
		printf("cannot receive data \n");
	}
	  
	/* print received message */
	printf("from %s:UDP%u : \n", 
	   inet_ntoa(cliAddr.sin_addr),
	   ntohs(cliAddr.sin_port));
}

void SocketClient::OpenSenderSocket(const char * hostName, int port) 
{

	/* get server IP address (no check if input is IP address or DNS name */
	h = gethostbyname(hostName);
	if(h==NULL)
	{
		printf("unknown host '%s' \n", hostName);
		exit(1);
	}
  
	/* socket creation */
	_socketDescripter = socket(AF_INET,SOCK_DGRAM,0);
	if(_socketDescripter<0) {
		printf("cannot open socket \n");
		exit(1);
	}
  
	/* bind any port */
	struct sockaddr_in cliAddr;
	cliAddr.sin_family = AF_INET;
	cliAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	cliAddr.sin_port = htons(0);
  
	int bindResult = bind(_socketDescripter, (struct sockaddr *) &cliAddr, sizeof(cliAddr));
	if(bindResult<0) {
		printf("cannot bind port\n");
		exit(1);
	}
  
	remoteServAddr.sin_family = h->h_addrtype;
	memcpy((char *) &remoteServAddr.sin_addr.s_addr, 
	h->h_addr_list[0], h->h_length);
	remoteServAddr.sin_port = htons(port);
}

void SocketClient::Send(char *quad, int length)
{
	printf("sending data to '%s' (IP : %s) \n", h->h_name,
		inet_ntoa(*(struct in_addr *)h->h_addr_list[0]));
		   
	  /* send data */
	int bindResult = sendto(_socketDescripter, quad, length, 0,    
		(struct sockaddr *) &remoteServAddr, sizeof(remoteServAddr));

	if(bindResult<0) 
	{
		printf("cannot send data\n");
		close(_socketDescripter);
		exit(1);
	}
}

