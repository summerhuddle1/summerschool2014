#include "CastCalculator.h"
#include <cmath>

namespace
{
	double square(double d)
	{
		return d * d;
	}	

	double square_root(double d)
	{
		return pow(d, 0.5);
	}
}

Vec3 getVelocity(const Vec3 & acceleration, const Vec3 & currentVelocity, const double timeInterval)
{ 
	// v = u + at
	double x = currentVelocity.x + (acceleration.x * timeInterval);
	double y = currentVelocity.y + (acceleration.y * timeInterval);
	double z = currentVelocity.z + (acceleration.z * timeInterval);
	
	return Vec3(x,y,z);
}
		
double getSpeed(const Vec3 & velocity)
{
	// 3D Pythagoras Theorem
	return square_root(square(velocity.x) + square(velocity.y) + square(velocity.z));
}

