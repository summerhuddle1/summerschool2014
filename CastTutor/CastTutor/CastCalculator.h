#ifndef TO_METERS_PER_SECOND_INCLUDED
#define TO_METERS_PER_SECOND_INCLUDED

#include "Vec3.h"
#include "AccelerometerInterface.h"

Vec3 getVelocity(const Vec3 & acceleration, const Vec3 & currentVelocity, const double timeInterval);
double getSpeed(const Vec3 & velocity);

#endif
