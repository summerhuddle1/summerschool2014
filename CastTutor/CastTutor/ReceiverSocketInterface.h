#pragma once

#include <cstddef>

class ReceiverSocketInterface
{
public: 
	virtual ~ReceiverSocketInterface() {}
	virtual void OpenReceiverSocket() = 0;
	virtual void Receive(void *dataBuffer, size_t length) = 0;
};

