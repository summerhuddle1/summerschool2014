#ifndef CAST_EXPORTER_INCLUDED
#define CAST_EXPORTER_INCLUDED

#include "Quad.h"
#include <vector>
#include <ostream>

void printTimeAndSpeed(std::ostream & os, std::vector<Quad> & rawAcceleration);
void sortVectorByQuadIdentifier(std::vector<Quad> & rawAcceleration);

#endif
