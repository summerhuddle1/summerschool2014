#pragma once

#include "ReceiverSocketInterface.h"
#include "SenderSocketInterface.h"

#include <arpa/inet.h>
#include <netdb.h>

#define REMOTE_SERVER_PORT 1500
#define MAX_MSG 100

class SocketClient : public SenderSocketInterface, public ReceiverSocketInterface
{
public:      
	~SocketClient();
	virtual void OpenReceiverSocket();
	virtual void Receive(void *dataBuffer, size_t length);
	virtual void OpenSenderSocket(const char *hostName, int port);
	virtual void Send(char *quad, int length);
	
private:
	int _socketDescripter;
    struct sockaddr_in remoteServAddr;
    struct hostent *h;
};
