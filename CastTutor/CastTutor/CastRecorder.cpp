#include "CastRecorder.h"
#include "Quad.h"
#include "CastExporter.h"

#include <vector>
#include <fstream>

CastRecorder::CastRecorder(ReceiverSocketInterface *socket)
	: _socket(socket)
{
}

void CastRecorder::CaptureCast()
{
	std::vector<Quad> cast;
	Quad q;
	do
	{
		_socket->Receive(&q, sizeof(q));
		if (q.identifier != 0)
		{
			cast.push_back(q);
		}
	}
	while (q.identifier != 0);
	
	std::ofstream data_csv("data.csv");
	printTimeAndSpeed(data_csv, cast);
}
