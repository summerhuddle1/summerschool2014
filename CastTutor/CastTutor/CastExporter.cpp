#include "CastExporter.h"
#include "Vec3.h"
#include "CastCalculator.h"
#include "AccelerometerInterface.h"
#include <algorithm>

using namespace std;

namespace
{
	bool comparisonOfQuadsByIdentifier(const Quad & lhs, const Quad & rhs) 
	{
		return lhs.identifier < rhs.identifier;
	}
	
	double convert(short value)
	{ //128 to convert -512 to 511 to +-4, 9.80665 = g
		return (double)value / 128.0 * 9.80665;
	}
}

void sortVectorByQuadIdentifier(vector<Quad> & raw)
{
	sort(raw.begin(), raw.end(), comparisonOfQuadsByIdentifier);
}

void printTimeAndSpeed(std::ostream & os, vector<Quad> & raw)
{
	sortVectorByQuadIdentifier(raw);

	os << "date,close" << endl << "0,0" << endl; // header for the PHP code and (0,0).
	double time = 0.0;
	double timeInterval = 0;
	Vec3 currentVelocity(0.0, 0.0, 0.0);
	for(size_t i = 1; i != raw.size(); i++)
	{
		time = (raw[i].identifier - raw[0].identifier)/ 100.0;
		timeInterval = (raw[i].identifier - raw[i-1].identifier)/ 100.0;
		Vec3 acceleration(convert(raw[i].x), convert(raw[i].y), convert(raw[i].z));
		currentVelocity = getVelocity(acceleration, currentVelocity, timeInterval);
		double speed = getSpeed(currentVelocity);
		
		os << time << ',' << speed << endl;
	}
}

