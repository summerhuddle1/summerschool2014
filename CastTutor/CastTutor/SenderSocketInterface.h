#pragma once


class SenderSocketInterface
{
public: 
	virtual ~SenderSocketInterface() {}
	virtual void OpenSenderSocket(const char *hostName, int port) = 0;
	virtual void Send(char *quad, int length) = 0;
};
