#include <gtest/gtest.h>

#include "Drivers/AK8975Compass.hpp"
#include "MockHAL/MockI2C.hpp"

const unsigned char deviceAddress = 0x0C;

TEST(AK8975Compass, Reading_the_compass_all_axis_0)
{
	// Given
	const int maxOperations = 10;
	MockI2COperation operations[maxOperations];
	MockI2C i2c(operations, maxOperations);
	AK8975Compass target(i2c);

	const unsigned char readCommand[] = { 0x03 };
	const unsigned char readData[] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

	i2c.ExpectSetAddress(deviceAddress, true);
	i2c.ExpectWrite(readCommand, sizeof(readCommand), true);
	i2c.ExpectRead(readData, sizeof(readData), true);

	// When
	Raw3DSensorData result = target.ReadDirection();

	// Then
	i2c.Verify();
	EXPECT_EQ(0, result.x);
	EXPECT_EQ(0, result.y);
	EXPECT_EQ(0, result.z);
}

TEST(AK8975Compass, Reading_the_compass_all_axis_different)
{
	// Given
	const int maxOperations = 10;
	MockI2COperation operations[maxOperations];
	MockI2C i2c(operations, maxOperations);
	AK8975Compass target(i2c);

	const unsigned char readCommand[] = { 0x03 };
	const unsigned char readData[] = { 0x03, 0x00, 0xFF, 0xFF, 0x00, 0x80 };

	i2c.ExpectSetAddress(deviceAddress, true);
	i2c.ExpectWrite(readCommand, sizeof(readCommand), true);
	i2c.ExpectRead(readData, sizeof(readData), true);

	// When
	Raw3DSensorData result = target.ReadDirection();

	// Then
	i2c.Verify();
	EXPECT_EQ(3, result.x);
	EXPECT_EQ(-1, result.y);
	EXPECT_EQ(-32768, result.z);
}

TEST(AK8975Compass, Starting_a_new_reading)
{
	// Given
	const int maxOperations = 10;
	MockI2COperation operations[maxOperations];
	MockI2C i2c(operations, maxOperations);
	AK8975Compass target(i2c);

	const unsigned char initialiseCommand[] = { 0x0A, 0x01 };

	i2c.ExpectSetAddress(deviceAddress, true);
	i2c.ExpectWrite(initialiseCommand, sizeof(initialiseCommand), true);

	// When
	target.StartDirectionRead();

	// Then
	i2c.Verify();
}

TEST(AK8975Compass, Checking_if_the_reading_is_ready_when_it_is_not_ready)
{
	// Given
	const int maxOperations = 10;
	MockI2COperation operations[maxOperations];
	MockI2C i2c(operations, maxOperations);
	AK8975Compass target(i2c);

	const unsigned char readStatusRegisterCommand[] = { 0x02 };
	const unsigned char readStatusRegisterIncomplete[] = { 0x00 };

	i2c.ExpectSetAddress(deviceAddress, true);
	i2c.ExpectWrite(readStatusRegisterCommand, sizeof(readStatusRegisterCommand), true);
	i2c.ExpectRead(readStatusRegisterIncomplete, sizeof(readStatusRegisterIncomplete), true);

	// When
	EXPECT_FALSE(target.IsReadingComplete());

	// Then
	i2c.Verify();
}

TEST(AK8975Compass, Checking_if_the_reading_is_ready_when_it_is_ready)
{
	// Given
	const int maxOperations = 10;
	MockI2COperation operations[maxOperations];
	MockI2C i2c(operations, maxOperations);
	AK8975Compass target(i2c);

	const unsigned char readStatusRegisterCommand[] = { 0x02 };
	const unsigned char readStatusRegisterComplete[] = { 0x01 };

	i2c.ExpectSetAddress(deviceAddress, true);
	i2c.ExpectWrite(readStatusRegisterCommand, sizeof(readStatusRegisterCommand), true);
	i2c.ExpectRead(readStatusRegisterComplete, sizeof(readStatusRegisterComplete), true);

	// When
	EXPECT_TRUE(target.IsReadingComplete());

	// Then
	i2c.Verify();
}
