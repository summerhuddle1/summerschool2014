#pragma once
#include "Wristband/SocketInterface.h"

class MockSocketClient : public SocketInterface
{
public:
	void Open(const char *hostName, int port)
	{
		_mockHostName = hostName;
		_mockPort = port;
	}
	void Send(char *data, int ){_mockData = data;};
	
	const char *_mockHostName;
	char *_mockData;
	int _mockPort;
};
