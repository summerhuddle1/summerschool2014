#pragma once
#include "SocketInterface.h"
#include "WristbandQuad.h"

class BMA150DataConverter
{
public:
	BMA150DataConverter(SocketInterface *socket);
	
	void SendtoSocket(Quad *quad);
	Quad ConvertBytesToQuad(unsigned char bytes[]);
	
	SocketInterface * socket;

private:
	long id;
};
