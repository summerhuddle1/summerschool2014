#include "BMA150DataConverter.h"
#include <stdio.h>

BMA150DataConverter::BMA150DataConverter(SocketInterface * s) 
	: socket(s)
	, id(0)
{
}

void BMA150DataConverter::SendtoSocket(Quad * quad)
{
	socket->Send((char *)quad, sizeof(Quad));
}

Quad BMA150DataConverter::ConvertBytesToQuad(unsigned char bytes[])
{
	id++;
	if (id == 0) 
		id++;

	short * raw = (short*)bytes;

	Quad result(id,
				raw[0] >> 6,
				raw[1] >> 6,
				raw[2] >> 6);
	
	return result;
}
