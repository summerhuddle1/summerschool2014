#pragma once

#include "WristbandQuad.h"
#include "SocketInterface.h"

#include <arpa/inet.h>
#include <netdb.h>


#define REMOTE_SERVER_PORT 1500
#define MAX_MSG 100

class SocketClient : public SocketInterface
{
public:      
	void Open(const char *hostName, int port);
	void Send(char *data, int length);
	
private:
	int socketDescripter, bindResult;
    struct sockaddr_in cliAddr, remoteServAddr;
    struct hostent *h;
};
