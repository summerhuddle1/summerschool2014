#pragma once

class SocketInterface
{
public:
	virtual void Open(const char *, int) = 0;
	virtual void Send(char *, int length) = 0;
};
