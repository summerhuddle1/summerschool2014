#pragma once

#include "CastTutor/ReceiverSocketInterface.h"
#include "CastTutor/SenderSocketInterface.h"
#include "CastTutor/Quad.h"

#include <vector>

class MockSocketClient : public ReceiverSocketInterface, public SenderSocketInterface
{
public:
	MockSocketClient();
	~MockSocketClient();
	
	virtual void OpenReceiverSocket(){ }
	virtual void OpenSenderSocket(const char *, int ){ }
	virtual void Send(char *quad, int length);
	virtual void Receive(void *, size_t);              // SHOULD return an int
	virtual void expect(const Quad & q);
	
private:
	void assertAllFakeQuadsSent() const;
	size_t index;
	std::vector<Quad> quads;
	
	char *_mockHostName;
	char *_mockQuad;
	int _mockPort;

};
