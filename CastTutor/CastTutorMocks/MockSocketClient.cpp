#include "MockSocketClient.h"

#include <gtest/gtest.h>
#include <string.h>

MockSocketClient::MockSocketClient()
	: index(0)
{
}

MockSocketClient::~MockSocketClient()
{
	assertAllFakeQuadsSent();
}

void MockSocketClient::expect(const Quad & q)
{
	quads.push_back(q);
}

void MockSocketClient::Receive(void * buffer, size_t n)
{
	EXPECT_GE(n, sizeof(Quad));
	EXPECT_LT(index, quads.size());

	if (n >= sizeof(Quad) && index < quads.size())
	{	
		memcpy(buffer, &quads[index], sizeof(Quad));
		index++;
	}
}

void MockSocketClient::Send(char *quad, int)
{
	_mockQuad = quad;
}

void MockSocketClient::assertAllFakeQuadsSent() const
{
	ASSERT_EQ(quads.size(), index);	
}
