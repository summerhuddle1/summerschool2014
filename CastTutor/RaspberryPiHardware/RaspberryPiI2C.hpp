#ifndef _RASPBERRYPII2C_H
#define _RASPBERRYPII2C_H

#include "HAL/I2C.hpp"

class RaspberryPiI2C : public I2C
{
private:
	int file;
public:
	RaspberryPiI2C();
	virtual ~RaspberryPiI2C();
	virtual bool SetAddress(unsigned char address);
	virtual bool Read(void * buffer, int length);
	virtual bool Write(const void * buffer, int length);
};

#endif





