#pragma once

#include "HAL/I2C.hpp"
#include "Accelerometer.hpp"

class BMA150Accelerometer : public Accelerometer
{
private:
	I2C& _i2c;
public:
	explicit BMA150Accelerometer(I2C& i2c);

	virtual Raw3DSensorData ReadAcceleration() const;
};

class BMA150AccelerometerCommunicationsError : public AccelerometerException
{
};
