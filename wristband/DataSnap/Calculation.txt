struct vec3
{
	float x;
	float y;
	float z;
};

------------------------------------------------------------------------------------------------------

class DataSnippet
{
Public:
	vec3 Acceleration;
	vec3 Velocity;
	float Speed;

Private:
	void getVelocity(vec3 previousVelocity);
	void getSpeed();
};

------------------------------------------------------------------------------------------------------

vec3 previousVelocity = 0;

void DataSnippet::getVelocity(vec3 previousVelocity)
{
	/* DataSnippetName */.Velocity.x = ((previousVelocity.x)*Time) + (0.5*(/* DataSnippetName */.Acceleration.x)*(Time^2));
	/* DataSnippetName */.Velocity.y = ((previousVelocity.y)*Time) + (0.5*(/* DataSnippetName */.Acceleration.y)*(Time^2));
	/* DataSnippetName */.Velocity.z = ((previousVelocity.z)*Time) + (0.5*(/* DataSnippetName */.Acceleration.z)*(Time^2));
	
	previousVelocity.x = /* DataSnippetName */.Velocity.x;
	previousVelocity.y = /* DataSnippetName */.Velocity.y;
	previousVelocity.z = /* DataSnippetName */.Velocity.z;
};

------------------------------------------------------------------------------------------------------

void DataSnippet::getSpeed()
{
	/* DataSnippetName */.Speed = (((/* DataSnippetName */.Velocity.x)^2)+((/* DataSnippetName */.Velocity.y)^2)+((/* DataSnippetName */.Velocity.z)^2))^(0.5);
};